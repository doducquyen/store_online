<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Cate;
use App\Product;
use App\ProductImages;
use Input, File;

class ProductController extends Controller {

	public function getList() {
		$data = Product::select('id','name','price','cate_id','created_at')->orderBy('id','DESC')->get()->toArray();
		return view('admin.product.list',compact('data'));
	}

	public function getAdd() {
		$cate = Cate::select('id','name','parent_id')->get()->toArray();
		return view('admin.product.add', compact('cate'));
	}

	public function postAdd(ProductRequest $request) {
		$file_name = $request->file('fImages')->getClientOriginalName();
		$product = new Product();
		$product->name = $request->txtName;
		$product->alias = changeTitle($request->txtName);
		$product->price = $request->txtPrice;
		$product->intro = $request->txtIntro;
		$product->content = $request->txtContent;
		$product->image = $file_name;
		$product->keywords = $request->txtKeywords;
		$product->description = $request->txtDescription;
		$product->user_id = 1;
		$product->cate_id = $request->sltCateParent;
		$request->file('fImages')->move('resources/upload/',$file_name);
		$product->save();
		$product_id =$product->id;
		if(Input::hasFile('fProductDetail')) {
			foreach (Input::file('fProductDetail') as $file) {
				$product_image = new ProductImages();
				if(isset($file)) {
					$product_image->image = $file->getClientOriginalName();	
					$product_image->product_id = $product_id;
					$file->move('resources/upload/detail/', $product_image->image);
					$product_image->save();
				}
			}
		}
		return redirect()->route('admin.product.getList')->with(['flash_level' => 'success', 'flash_message' => 'Add a product success']);
	}

	public function getDelete($id) {
		$product_detail = Product::find($id)->pimages->toArray();
		foreach ($product_detail as $val) {
			File::delete('resources/upload/detail/'.$val['image']);
		}
		$product = Product::find($id);
		File::delete('resources/upload/'.$product->image);
		$product->delete($id);
		return redirect()->route('admin.product.getList')->with(['flash_level' => 'success', 'flash_message' => 'Delete a product success']);
	}
}
